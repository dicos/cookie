<?php
$value = $_COOKIE["USER_ID"];
if(!$value) {
    $this_date = date("Y-m-d H:i:s", time());
    $data = $_SERVER["REMOTE_ADDR"] . " " . rand() . $this_date;
    $value = $this_date . " " . sha1($data);
    $next_date = mktime(0, 0, 0, 1, 1, 2100);
    setcookie("USER_ID", $value, $next_date);
}
header('Content-Type: application/json');
?>
var USER_ID = "<?php echo $value?>";
